<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {   
        //grab all guards
        $guards = empty($guards) ? [null] : $guards;
        /*
        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return redirect(RouteServiceProvider::HOME);
            }
        }*/

        //for each guard
        foreach($guards as $guard){
            //create a switch statement
            switch ($guard){
                //if the guard is the admin guard
                case "admin":
                    //check if the admin guard is authenticated
                    if(Auth::guard($guard)->check()){
                        //if the admin guard is authenticated then redirect to the admin dashboard
                        return redirect()->route("admin.dashboard");
                    }
                    //break out of the switch statement
                    break;
                //if the guard is not the admin guard use this as the default setting for handling the web guard
                default:
                    //if the web guard is authenticated
                    if(Auth::guard($guard)->check()){
                        //if the web guard is authenticated then redirect to the admin dashboard 
                        return redirect()->route("home");
                    }
                    //break out of the switch statement
                    break;
            }
        }

        //if neither of the guards are authenticated then redirect to the next request
        return $next($request);
    }
}
