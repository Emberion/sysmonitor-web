<?php

namespace App\Http\Controllers;
//load in the auth facade
use Auth;
use Illuminate\Http\Request;
//use the ticket model
use App\Models\Ticket;
//use the TicketComment model
use App\Models\TicketComment;

class TicketCommentsController extends Controller
{
    //the constructor
    public function __construct(){
        //check if a user/admin is authenticated
        $this->middleware("auth:web,admin");
    }

    //method responsible for checking if the ticket belongs to the authenticated user using the web guard
    public function check_user_authorization(){
        return 123;
    }

    //method responsible for checking if the user is logged in as an admin @returns bool
    public function check_admin(){
        //if the user is logged in using the admin guard
        if(Auth::guard("admin")->check()){
            //return true since the user is an admin
            return true;
        //if the user is not logged in using the admin guard
        }else{
            //return false since the user is not an admin
            return false;
        }
    }

    //method responsible for posting a comment to the specified ticket
    public function post_comment(Request $request){
        //grab the ticket id from the request
        $ticket_id = $request->ticket_id;
        //if the user is logged in with the admin web guard so as an admin
        if($this->check_admin() == true){
            //grab the admin id
            $admin_id = Auth::guard("admin")->user()->id;
            //grab the ticket
            $ticket = Ticket::where("id","=","$ticket_id")->first();
            //check if the ticket exists
            if($ticket !== null){
                //get the comment message from the post request
                $message = $request->message;
                //create a new TicketComment object
                $comment = new TicketComment;
                //set the admin id
                $comment->user_admin_id = $admin_id;
                //set the comment message
                $comment->message = $message;
                //set the is_admin column;
                $comment->is_admin = "true";
                //set the ticket id to which this comment belongs
                $comment->ticket_id = $ticket->id;
                //save the comment to the ticket_comments table 
                $comment->save();
                //redirect back to the ticket page
                return redirect()->route("admin-load-user-ticket",["id" => "$ticket_id"]);
            //if the ticket does not exist
            }else{
                //redirect back to the admin tickets page and pass along an error notification 
                return redirect()->route("admin-tickets")->with(["notification" => ["error" => "Ticket with that id does not exist"]]);
            }
        //if the user is logged in as a regular user
        }else{
            //get the authenticated user id using the web guard
            $user_id = Auth::guard("web")->user()->id;
            //get the ticket the comment is to be placed on
            $ticket = Ticket::where("id","=","$ticket_id")->first();
            //if the ticket exists
            if($ticket !== null){
                //if the ticket does not belong to the specified user using the web guard
                if($ticket->user_id !== $user_id){
                    //redirect back to the user tickets overview page with an error notification since the user is not allowed to comment on this ticket
                    return redirect()->route("user-tickets")->with(["notification" => ["error" => "Unauthorized comment request"]]);
                }
                //get the comment message from the post request
                $message = $request->message;
                //create a new TicketComment object
                $comment = new TicketComment;
                //set the user id
                $comment->user_admin_id = $user_id;
                //set the comment message
                $comment->message = $message;
                //set the is_admin column;
                $comment->is_admin = "false";
                //set the ticket id to which this comment belongs
                $comment->ticket_id = $ticket->id;
                //save the comment to the ticket_comments table 
                $comment->save();
                //redirect back to the ticket page so the page is updated
                return redirect()->route("load-user-ticket",["id" => "$ticket_id"]);
            //if the specified ticket does not exist
            }else{
                //redirect back to the user tickets overview page with an error notification since the ticket does not exist.
                return redirect()->route("user-tickets")->with(["notification" => ["error" => "Ticket does not exist"]]);
            }

        }
    }
}
