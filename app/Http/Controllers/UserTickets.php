<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//load in the ticket model
use App\Models\Ticket;
//load in the ticket comments model
use App\Models\TicketComment;
//use the auth facade
use Auth;

class UserTickets extends Controller
{
    //the contruct
    public function __construct(){
        //set the user tickets controller to only allow user to access it when logged in with the web guard
        $this->middleware("auth:web");
    }

    //the method responsible for loading the specified user tickets page.
    public function load_tickets(){
        //grab the authenticated user's id using the web guard
        $user_id = Auth::guard("web")->user()->id;
        //grab the user's tickets and sort them by id in descending order
        $user_tickets = Ticket::select("id","user_id","status","title","message")->where("user_id","=","$user_id")->orderBy("id","DESC")->get();
        //load the user tickets page and pass along all required data
        return view("tickets.user-tickets-page")->with("data",["user_tickets" => $user_tickets]);
    }

    //method responsible for loading the create ticket page @returns view
    public function load_create_ticket_page(){
        //load the create ticket page view and pass along the required data
        return view("tickets.create-ticket-page");
    }

    //method for saving the ticket data to the database
    public function submit_ticket(Request $request){
        //validate if both the ticket title and message have been provided by the user
        $request->validate([
            "title" => "required",
            "content" => "required"
        ]);
        //create a new ticket object
        $ticket = new Ticket;
        //set ticket user id to the id of the current logged in user using the web guard
        $ticket->user_id = Auth::guard("web")->user()->id;
        //set the ticket title to the request title
        $ticket->title = $request->title;
        //set the ticket message to the request content
        $ticket->message = $request->content;
        //set the ticket status to open
        $ticket->status = "open";
        //save the ticket to the database
        $ticket->save();
        //redirect to the user's tickets listing page and pass along a notification letting the user know their ticket was created successfully
        return redirect()->route("user-tickets")->with(["notification" => ["success" => "Ticket created a admin will look into your issue as soon as possible."]]);
    }
    
    //method responsible for opening the specified user ticket for that specific user @takes int @returns view
    public function load_ticket($id){
        //grab the user's id using the web guard
        $user_id = Auth::guard("web")->user()->id;
        //try grabbing the ticket with the specified id
        $ticket = Ticket::where("id","=","$id")->first();
        //if the ticket does not exist
        if($ticket == null){
            //redirect to the user's tickets page with an error saying the specified ticket does not exist
            return redirect()->route("user-tickets")->with(["notification" => ["error" => "Specified ticket does not exist"]]);
        }
        //if the ticket exists but does not belong to the user that is logged in
        if($ticket->user_id !== $user_id){
            //redirect to the user's tickets page with an error saying unauthorized request.
            return redirect()->route("user-tickets")->with(["notification" => ["error" => "Unauthorized request"]]);
        }
        //grab the specified ticket's comments if they exist REMOVE WHEN MODEL RELATIONS ARE TESTED AND WORKING
        //$ticket_comments = $this->load_ticket_comments($id);
        //if the ticket belongs to this user and exists load in this view and pass along the ticket and it's comments as the required data
        return view("tickets.load-user-ticket")->with("data",["ticket" => $ticket,"comments" => $ticket->comments(),"user" => Auth::guard("web")->user()]);
    }

    //method responsible for loading the specified ticket's comments REMOVE THEN MODEL RELATIONS ARE TESTED AND WORKING
    public function load_ticket_comments($id){
        //grab the specified ticket's comments load them in descending order by id
        $ticket_comments = Ticket::where("id","=","$id")->first()->comments();//TicketComment::where("ticket_id","=","$id")->orderBy("id","DESC")->get();
        //return the laravel collection containing all the specified ticket's comments
        return $ticket_comments;
    }
}
