<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//load in the Server model
use App\Models\Server;
//load in the auth functionality
use Auth;
//load in session functionality
use Session;

class ServersList extends Controller
{
    //use the constructor to make sure users accessing this controller's methods are signed in as a user
    public function __construct(){
        $this->middleware("auth:web");
    }

    //load the servers list page
    public function load_servers_list(){
        //get the redirect data if applicable
        $notification_message = Session::get("notification");
        //get the current logged in user's ID;
        $user_id = Auth::guard("web")->user()->id;
        //grab all the servers regarding the specified user.
        $user_servers = Server::where("user_id","=","$user_id")->paginate(15);
        //load the view and pass it the user's server list
        return view("server-overview.server-overview-page")->with("data",["notification" => $notification_message,"pagetype"=>"user","servers"=>$user_servers]);
    }

    //method that loads the add server form
    public function load_add_server_form(){
        //open the add server form
        return view("server-overview.add-server-form")->with("data",["pagetype"=>"user"]);
    }

    //method that checks if the ip address is a valid ipv4 or ipv6 ip address @returns bool
    public function validate_ip($ip){
        //return true if the ip is a valid ip and false if it is not a valid ip address
        return filter_var($ip,FILTER_VALIDATE_IP);
    }

    //method that handles the post request and adds the newly added server to the database
    public function handle_server_addition(Request $request){
        //validate the ip entered by the user and make sure it has been entered
        $request->validate([
            "ip" => "required"
        ]);
        //if the ip address given is not a valid ipv4 or ipv6 address then return a redirect back to the servers page with an error message
        if($this->validate_ip($request->ip) == false){
            //redirect back to the servers list page and show an error message
            return redirect()->route("serverslist")->with("notification",["error" => "This is not a valid ipv4 or ipv6 address please try again."]);
        }
        //create a new server row object
        $row = new Server;
        //set the server_ip column to the ip address entered by the user
        $row->server_ip = $request->ip;
        //set the user_id column to the user's id
        $row->user_id = Auth::guard("web")->user()->id;
        //set the activated column to false
        $row->activated = "false";
        //save the row to the database
        $row->save();
        //return a redirect and pass the notification data along side of the redirect so a notification can be shown on the page the server redirects to
        return redirect()->route("serverslist")->with(["notification" => ["success" => "Server added successfully"]]);
    }

    //method for deleting a server from the user's server list
    public function remove_server(Request $request){
        //get the authenticated user id check using the web guard
        $user_id = Auth::guard("web")->user()->id;
        //get the server row by it's id
        $server_row = Server::select("user_id","id")->where("id","=","$request->id")->first();
        //if a server row is found so is not null
        if(isset($server_row)){
            //if the user id matches the user id column of the server row
            if($user_id == $server_row->user_id){
                //delete the server row from the database
                $server_row->delete();
                //return true as the response since then we can use that specific response in ajax to update the user server row list dynamically
                return true;
            }
        }
        //return false as the response since then we can use that specific response in ajax to know nothing needs to be updated(most likely a forged request)
        return false;
    }
}
