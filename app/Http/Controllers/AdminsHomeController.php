<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdminsHomeController extends Controller
{
    ///constructor
    public function __construct(){
        //only allow users using the authenticated auth middleware to use this controller functionality
        $this->middleware("auth:admin");
    } 

    //load the admin home page
    public function load_admin_dashboard(){
        return view("admin.admin-dashboard")->with(["name" => Auth::guard("admin")->user()->name]);
    }
}
