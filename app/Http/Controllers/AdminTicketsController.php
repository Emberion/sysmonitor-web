<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Models\User;
use Auth;

class AdminTicketsController extends Controller
{
    //the constructor holding the middleware allowing only authenticated admins to use this controller
    public function __construct(){
        $this->middleware("auth:admin");
    }

    //method that loads and renders the admin tickets page
    public function load_admin_tickets(){
        //grab all available tickets
        $tickets = Ticket::orderBy("id","DESC")->paginate(15);
        //return the view rendering that leads to the admin tickets overview page
        return view("tickets.admin-tickets-page")->with("data",["tickets" => $tickets]);
    }

    //method that loads a specific user's ticket @takes int
    public function admin_load_user_ticket($id){
        //grab the ticket by id
        $ticket = Ticket::where("id","=","$id")->first();
        //grab the admin user object
        $user = Auth::guard("admin")->user();
        //if the ticket does exist
        if($ticket !== null){
            //grab the ticket comments
            $comments = $ticket->comments();
            //return the view with the ticket as data
            return view("tickets.admin-load-user-ticket")->with("data",["ticket" => $ticket,"user" => $user,"comments" => $comments]);
        //if the ticket does not exist
        }else{
            //redirect back to the admin tickets page and show an error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error" => "Ticket with that id does not exist."]]);
        }
    }

    //method that is responsible for changing the ticket status to open or closed @takes integer
    public function change_ticket_status($id){
        //get the ticket id passed in the GET request
        $ticket_id = $id;
        //get the ticket using the specified id
        $ticket = Ticket::where("id","=","$id")->first();
        //check if the current ticket exists
        if($ticket !== null){
            //if the ticket status is open
            if($ticket->status == "open"){
                //change the ticket status to closed
                $ticket->status = "closed";
            //if the ticket status is closed
            }elseif($ticket->status == "closed"){
                //change the ticket status to open
                $ticket->status = "open";
            }
            //save the changes to the ticket status to the database
            $ticket->save();
            //return a redirect back to the admin ticket overview page with a success notification
            return redirect()->route("admin-tickets")->with(["notification" => ["success" => "Ticket status successfully changed"]]);
        //if the ticket does not exist
        }else{
            //redirect back to the admin tickets overview with an error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error","Ticket with that id does not exist"]]);
        }
    }

    //method responsible for deleting a specified user's ticket as an admin @takes request @returns redirect
    public function delete_user_ticket($id){
        //grab the ticket from the database
        $ticket = Ticket::where("id","=","$id")->first();
        //if the ticket exists
        if($ticket !== null){
            //run the method that deletes all the comments bound to this ticket
            $this->delete_ticket_comments($id);
            //delete the ticket row from the database
            $ticket->delete();
            //redirect back to the admin tickets overview with a success notification
            return redirect()->route("admin-tickets")->with(["notification" => ["success","Ticket successfully deleted"]]);
        //if the ticket does not exist
        }else{
            //redirect back to the admin tickets overview with a error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error","Ticket with that ID does not exist"]]);
        }
        
    }

    //method responsible for deleting all comments for a specified ticket so when a ticket is deleted the comments are deleted also @takes integer @returns bool
    public function delete_ticket_comments($ticket_id){
        //grab all the specified ticket's comments in a laravel collection
        $ticket_comments = Ticket::where("id","=","$ticket_id")->first()->comments();
        //for each comment that is bound to this ticket
        foreach($ticket_comments as $comment){
            //delete the comment
            $comment->delete();
        }
        //return true
        return true;
    }

    //method to find tickets by username @takes string @returns view
    public function find_username_tickets($username){
        //grab the user object using the User model
        $user = User::where("name","=",strtolower($username))->first();
        //if the result of the user variable is not null
        if($user !== null){
            //grab grab the user id from the grabbed user object
            $user_id = $user->id;
            //grab the user's tickets and paginate them per 15 results
            $tickets = Ticket::where("user_id","=","$user_id")->orderBy("id","DESC")->paginate(15);
            //load the admins tickets overview page and pass the tickets along as data
            return view("tickets.admin-tickets-page")->with("data",["tickets" => $tickets]);
        //if the result of the user variable is null
        }else{
            //return a redirect back to the admin tickets page and send along an error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error","User with that name does not exist"]]);
        }
    }

    //find ticket by title @takes string @returns view
    public function find_ticket_by_name($ticket_name){
        //set the ticket name to be completely lowercase
        $ticket_name = strtolower($ticket_name);
        //search the tickets table for the ticket by name using the Ticket model
        $ticket = Ticket::where("title","LIKE",'%'.$ticket_name.'%')->get();
        //if the ticket variable is not null
        if($ticket !== null){
            //return the view and pass along the ticket as data
            return view("tickets.admin-tickets-page")->with("data",["tickets" => $ticket]);
        //if the ticket is null so does not exist
        }else{
            //redirect back to the admins tickets page with an error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error","Ticket with that name does not exist"]]);
        }
    }

    //find ticket by id @takes int @returns view
    public function find_ticket_by_id($id){
        //find the ticket by id
        $ticket = Ticket::where("id","=","$id")->get();
        //if the ticket variable is not null so it exists
        if($ticket !== null){
            //return the view and pass along the ticket as data
            return view("tickets.admin-tickets-page")->with("data",["tickets" => $ticket]);
        //if the ticket is null so does not exist
        }else{
            //redirect back to the admins tickets page with an error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error","Ticket with that ID does not exist"]]);
        }
    }

    //method for selecting one of three search options and executing it
    public function select_search_category_and_search(Request $request){
        $select_value = $request->get("search_filter");
        // if the byID property has been set in the request
        if($select_value == "byID"){
            //run the find_ticket_by_id method
            return $this->find_ticket_by_id($request->filterSearch);
        // if the byName property has been set in the request
        }elseif($select_value == "byName"){
            //run the find_ticket_by_name method
            return $this->find_ticket_by_name($request->filterSearch);
        // if the byUserName property has been set in the request
        }elseif($select_value == "byUserName"){
            //run the find_username_tickets method
            return $this->find_username_tickets($request->filterSearch);
        //if no property or a wrong property has been set in the request
        }else{
            //redirect back to the admins tickets page and display an error notification
            return redirect()->route("admin-tickets")->with(["notification" => ["error","invalid search criteria"]]);
        }
    }
}
