<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Auth;

class AdminsController extends Controller
{
    public function __construct(){
        //$this->middleware("admin");
    }
    //
    public function admin_test(){
        if(Auth::guard("admin")->check() == false){
            return 111;
        }
        return 123;
    }
}
