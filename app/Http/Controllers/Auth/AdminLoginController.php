<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AdminLoginController extends Controller
{
    public function __construct(){
        $this->middleware("guest:admin");
    }

    //method to show the login form view
    public function showLoginForm(){
        //return the admin login view
        return view("auth.admin-login");
    }

    //method that handles the login of the admin accounts
    public function login(Request $request){
        //validate form data
        $this->validate($request,[
            "email" => "required",
            "password" => "required|min:6",
            ]);
        //attempt to log in
        if(Auth::guard("admin")->attempt(["email" => $request->email,"password" => $request->password],$request->remember)){
            //if successful then redirect to intended location
            return redirect()->route("admin.dashboard");
        }
        //if fail then redirect back to the admin login with the form data
        return redirect(route("admin.login"))->withInput($request->only("email","remember"));
    }


    
}
