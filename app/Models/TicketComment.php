<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    use HasFactory;
    protected $table = "ticket_comments";

    protected $fillable = [
        "user_admin_id",
        "message",
        "is_admin"
    ];

    //method creating a model relationship
    public function ticket(){
        //make sure the ticket comment belongs to a ticket
        return $this->belongsTo(Ticket::class);
    }
}
