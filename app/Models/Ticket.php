<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $table = "tickets";


    protected $fillable = [
        "user_id",
        "status"
    ];

    //method containing a model relationship named comments
    public function comments(){
        //set that each ticket has multiple comments, grab the comments by the ticket id column using the ticket id column also order the result by descending order by using the id column
        return $this->hasMany(TicketComment::class,"ticket_id","id")->get();
    }
}
