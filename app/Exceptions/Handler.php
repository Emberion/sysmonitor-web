<?php

namespace App\Exceptions;


use Exception;
use Request;
use Response;
use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Arr;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    
    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {   
        //if the request is made through the api use the standard handler logic
        if($request->expectsJson()){
            return response()->json(["error" => "Unauthenticated",401]);
        }

        //grab the current guard
        $guard = Arr::get($exception->guards(),0);
        
        //check the guard type using a switch statement
        switch($guard){
            //if the guard is the admin guard
            case "admin":
                //set the login variable to the route name for the admin login
                $login = "admin.login";
                //break out of the switch statement
                break;
            //set the default 
            default:
                //set the default login guard to use to the web guard
                $login = "login";
                //break out of the switch statement
                break;
        }
        //redirect to the login or admin login page
        return redirect()->guest(route($login));

    }
}
