/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { default: Axios } = require('axios');

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//Vue.component("remove-server-row",require("./VueLogic/RemoveServerRow.vue").default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data:{
        message: "test"
    },
    methods:{
        //remove this method later
        changeText(){
            this.message = "hello";
        },

        //method that removes a specified server row from the user's servers list
        removeRow(event){
            //grab the target element
            target_element = event.target;
            //grab the target element id so it can be sent along the post request
            target_id = target_element.id;
            //send a post request using axios
            Axios.post("/serverlist/remove_server",{
                //send the element id as target id data
                id:target_id
            //after the request has been sent collect the response
            }).then((response) => {
                //if the response data equals true or in other words 1
                if(response.data == 1){
                    //remove the target list element from the list
                    target_element.parentElement.remove()
                }
            //if an error occurs server side
            }, (error) => {
                //log the error inside of the console
                console.log(error);
            });
        }
    }
});

app.changeText();
