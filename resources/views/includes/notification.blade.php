@if(Session::has("notification"))
    @if(isset(Session::get("notification")["success"]))
        <div class="row">
            <div class="col-4 offset-4">
                <div class="alert alert-success text-center">
                    <p>{{Session::get("notification")["success"]}}</p>
                </div>
            </div>
        </div>

    @elseif(isset(Session::get("notification")["warning"]))
        <div class="row">
            <div class="col-4 offset-4">
                <div class="alert alert-warning text-center">
                    <p>{{Session::get("notification")["warning"]}}</p>
                </div>
            </div>
        </div>

    @elseif(isset(Session::get("notification")["error"]))
        <div class="row">
            <div class="col-4 offset-4">
                <div class="alert alert-danger text-center">
                    <p>{{Session::get("notification")["error"]}}</p>
                </div>
            </div>
        </div>
    @endif
@endif