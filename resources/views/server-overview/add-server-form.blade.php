@extends("layouts.app")


@section("content") 
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h3>Enter a new server</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-10">
                {{Form::open(array("route" => "upload-server-data"))}}
                    <div class="form-group">
                        {{Form::label("IP")}}
                        {!! Form::input("","ip","",array("class" => "form-control")) !!}
                        {!! Form::submit("Submit",array("class" => "btn btn-success")) !!}
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection