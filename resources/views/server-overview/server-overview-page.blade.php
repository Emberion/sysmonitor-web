@extends("layouts.app")



@section("content")
    <section id="serverlistsection">
        <div class="container">
            <div class="row">
                <div class="col-10">
                    <div>
                        <a href="{{route('AddServer')}}" class="btn btn-success">Add Server</a>
                    </div>
                    <hr />
                </div>
                <div class="col-10">
                    <ul id="serverrows">
                        @foreach($data["servers"] as $server)
                            <li><a href="#">{{$server->server_ip}}</a> <button id="{{$server->id}}" class="btn btn-danger btn-sm" v-on:click="removeRow">Delete</button></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection