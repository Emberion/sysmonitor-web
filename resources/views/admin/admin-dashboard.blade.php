@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <a href="{{route('admin-tickets')}}" class="btn btn-primary">Tickets</a>
            </div>
        </div>
    </div>
    <h2>Hello admin {{ucfirst($name)}}!</h2>
@endsection