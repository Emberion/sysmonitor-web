@extends("layouts.app")

@section("content")

    <section id="tickets-section">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <a class="btn btn-primary" href="{{route('create-ticket')}}">Create Ticket</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped">
                        <thead>
                            <th>Title</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            @foreach($data["user_tickets"] as $ticket)
                                <tr>
                                    <td><a href="{{route('load-user-ticket',['id' => $ticket->id])}}">{{ucwords($ticket->title)}}</a></td>
                                    <td>{{ucfirst($ticket->status)}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection