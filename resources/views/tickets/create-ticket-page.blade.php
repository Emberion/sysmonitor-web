@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                {{Form::open(array("route" => "submit-ticket"))}}
                    {{ Form::label("Create Ticket")}}
                    {!! Form::input("","title","",array("placeholder" => "Title","class" => "form-control")) !!}
                    {!! Form::textarea("content",null,array("placeholder" => "ticket content","class" => "form-control")) !!}
                    {!! Form::submit("Submit",array("class" => "btn btn-success")) !!}
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection