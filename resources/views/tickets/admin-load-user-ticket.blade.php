@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div>
                    @if($data["ticket"]->status == "open")
                        <a href="{{route('admin-change-ticket-status',['id' => $data['ticket']->id])}}" class="btn btn-danger">Close Ticket</a>
                    @else
                        <a href="{{route('admin-change-ticket-status',['id' => $data['ticket']->id])}}" class="btn btn-success">Open Ticket</a>
                    @endif
                        <a href="{{route('admin-tickets')}}" class="btn btn-primary">Back</a>
                        <!--<a href="" class="btn btn-danger float-right">Delete</a>-->
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#confirmModal">
                            Delete
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="confirmModalLabel"><b>Warning</b></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure you want to delete this ticket?
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{route('admin-remove-ticket',['id' => $data['ticket']->id])}}" class="btn btn-primary">Delete</a>
                                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <p style="font-weight:bold;">{{ucwords($data["ticket"]->title)}}</p>
                        <span>{{ ucfirst(App\Models\User::select("name")->where("id","=",$data["ticket"]->user_id)->first()->name) }}</span>
                    </div>
                    <div class="card-body">
                        <p>{{$data["ticket"]->message}}</p>
                    </div>
                </div>
                <!-- put comment cards here -->
                @foreach($data["comments"] as $comment)
                    <div class="card comment-card" style="margin-top:30px; margin-bottom:30px;">
                        <div class="card-header">
                            @if($comment->is_admin == "false")
                                <span>{{ucfirst(App\Models\User::select("name")->where("id","=","$comment->user_admin_id")->first()->name)}}</span>
                            @else
                                <span>{{ucfirst(App\Models\Admin::select("name")->where("id","=","$comment->user_admin_id")->first()->name . " *")}}</span>
                            @endif
                        </div>
                        <div class="card-body">
                            <p>{{$comment->message}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                {{Form::open(array("route" => "post-comment"))}}
                    {!! Form::textarea("message",null,array("class" => "form-control")) !!}
                    {!! Form::input("","ticket_id",$data["ticket"]->id,["hidden"]) !!}
                    {!! Form::submit("Comment",["class" => "btn btn-primary submit-button"]) !!}
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection