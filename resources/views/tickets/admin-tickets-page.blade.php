@extends("layouts.app")

@section("content")
    <div class="container">
        @if(strpos($_SERVER["REQUEST_URI"],"filter_search"))
            <div class="row">
                <div class="col-md-10">
                    <div class="btn-wrapper">
                        <a href="{{route('admin-tickets')}}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-10">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="filterDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filter
                    </button>
                    <!-- create modal for this -->
                    <div class="dropdown-menu">
                        <button class="dropdown-item">By ID</button>
                        <button class="dropdown-item">By Username</button>
                        <button class="dropdown-item">By Ticket Name</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                {!! Form::open(array("route" => "admin-ticket-filter-search")) !!}
                    {{Form::select("search_filter",["byID" => "ID","byName" => "name","byUserName" => "username"])}}
                    {{Form::input("","filterSearch","")}}
                    {{Form::submit("Search",["class" => ""])}}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                @foreach($data["tickets"] as $ticket)
                    <div class="card">
                        @if($ticket->status == "open")
                        <div class="card-body" style="background-color:#ff22ff;">
                        @else
                        <div class="card-body" style="background-color:#9efd38; border-color:#2f4c1a;">
                        @endif
                            <a href="{{route('admin-load-user-ticket',['id' => $ticket->id])}}">{{ucwords($ticket->title)}}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection