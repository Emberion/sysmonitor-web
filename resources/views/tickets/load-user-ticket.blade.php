@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div>
                    <a href="{{route('user-tickets')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <p style="font-weight:bold;">{{ucwords($data["ticket"]->title)}}</p>
                        <span>{{ucfirst(App\Models\User::select("name")->where("id","=",$data["ticket"]->user_id)->first()->name)}}</span>
                    </div>
                    <div class="card-body">
                        <p>{{$data["ticket"]->message}}</p>
                    </div>
                </div>
                <!-- put comment cards here -->
                @foreach($data["comments"] as $comment)
                    <div class="card comment-card" style="margin-top:30px; margin-bottom:30px;">
                        <div class="card-header">
                            @if($comment->is_admin == "false")
                                <span>{{ucfirst(App\Models\User::select("name")->where("id","=","$comment->user_admin_id")->first()->name)}}</span>
                            @else
                                <span>{{ucfirst(App\Models\Admin::select("name")->where("id","=","$comment->user_admin_id")->first()->name . " *")}}</span>
                            @endif
                        </div>
                        <div class="card-body">
                            <p>{{$comment->message}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                {{Form::open(array("route" => "post-comment"))}}
                    {!! Form::textarea("message",null,array("class" => "form-control")) !!}
                    {!! Form::input("","ticket_id",$data["ticket"]->id,["hidden"]) !!}
                    {!! Form::submit("Comment",["class" => "btn btn-primary submit-button"]) !!}
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection