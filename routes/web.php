<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get("/test_admin",[App\http\Controllers\AdminsController::class,"admin_test"]);


Route::prefix("serverlist")->group(function(){
    //get route to the users serverlist page
    Route::get("servers",[App\http\Controllers\ServersList::class,"load_servers_list"])->name("serverslist");
    //get road that loads the add server form
    Route::get("add_server",[App\http\Controllers\ServersList::class,"load_add_server_form"])->name("AddServer");
    //post route for adding the server to the database
    Route::post("upload_server_data",[App\http\Controllers\ServersList::class,"handle_server_addition"])->name("upload-server-data");
    Route::post("remove_server",[App\http\Controllers\ServersList::class,"remove_server"])->name("remove-server-data");
});

Route::prefix("admin")->group(function(){
    Route::get("/admin_login",[App\http\Controllers\Auth\AdminLoginController::class,"showLoginForm"])->name("admin.login");
    Route::get("/admin_dashboard",[App\http\Controllers\AdminsHomeController::class,"load_admin_dashboard"])->middleware("auth:admin")->name("admin.dashboard");
    Route::post("/admin_login_submit",[App\http\Controllers\Auth\AdminLoginController::class,"login"])->name("admin.login.submit");
    
});

Route::prefix("tickets")->group(function(){
    //get request for loading the specified user tickets page
    Route::get("/tickets_listing",[App\http\Controllers\UserTickets::class,"load_tickets"])->name("user-tickets");
    //get request for loading the admin tickets page
    Route::get("/admin_tickets_overview",[App\http\Controllers\AdminTicketsController::class,"load_admin_tickets"])->name("admin-tickets");
    //get request for loading the create ticket page
    Route::get("/create_ticket",[App\http\Controllers\UserTickets::class,"load_create_ticket_page"])->name("create-ticket");
    //post request for submitting the ticket data
    Route::post("/submit_ticket",[App\http\Controllers\UserTickets::class,"submit_ticket"])->name("submit-ticket");
    //post request for posting comments to a ticket
    Route::post("/post_comment",[App\http\Controllers\TicketCommentsController::class,"post_comment"])->name("post-comment");
    //post request to filter the search criteria for the admin's ticket filter search
    Route::post("/filter_search",[App\http\Controllers\AdminTicketsController::class,"select_search_category_and_search"])->name("admin-ticket-filter-search");
    //post request for removing a user ticket as an admin
    Route::get("/delete_user_ticket_admin/{id}",[App\http\Controllers\AdminTicketsController::class,"delete_user_ticket"])->name("admin-remove-ticket");
    //get request for loading a user ticket on the user side
    Route::get("/load_ticket/{id}",[App\http\Controllers\UserTickets::class,"load_ticket"])->name("load-user-ticket");
    //get request for loading a user ticket on the admin side
    Route::get("/load_ticket_admin/{id}",[App\http\Controllers\AdminTicketsController::class,"admin_load_user_ticket"])->name("admin-load-user-ticket");
    //get request for changing the ticket status using an admin account
    Route::get("/change_ticket_status/{id}",[App\http\Controllers\AdminTicketsController::class,"change_ticket_status"])->name("admin-change-ticket-status");
});